# A program to accommodate various activities for the day.

Features:

- Accommodates various activities for the day
- Able to supply the application with the “input data” via API Endpoint
- Import activities from file
- Edit the imported data
- Implented RESTful Api to calculate activites for teams.

Logic:

- Activities divided into various teams
- Each team will be performing various activities
- There might be a different ordering or combination of activities for the teams.
- Activity lengths are in minutes, or sprint (15 mins)
- No gap between each activity
- Activities must finish before deadline 05:00pm

This application serves a NodeJS RESTful API backend-application, including Unit Tests against the calculation endpoint. The API can be controlled by the included Angular6 frontend web-application, to provide comfortable functionality for the required accommodation. Please follow the instructions below, to start the application. 

The requested functionality from the exercise was implemented in NodeJS. The Angular6 web-application can provide a handful user interface, to control the API. The UI can import the provided test data, then we can send to the API for calculation.
The end-point is able to determinate, how many teams will be covered, also injects lunch break and presentation deadline. There is no time gap between the activities. An addition feature was implemented as a proof of concept 'activity-shaking', to prove that my solution is working. You can enable this feature by opt-in the check box next to the 'Accommodate' action button, when the data imported. Anytime we presses the button, the api service sends the data to the calculation endpoint immediately, then displays the processed data.

## Swagger API 
Design specification for this API [Swagger Accommodate API 1.0.0](https://app.swaggerhub.com/apis/ivanherczeg/Accommodate/1.0.0)

Use the provided `activities.txt` file from the root folder, and import with the web-application.

## Get the repo

`git clone https://gitlab.com/accenture-repo/node-angular6-activity-app`

## Installation 

Run `npm install` to install the application. 

## Run the application

Run `npm start` to start the API server with the web-application. Navigate to `http://localhost:3000` to access.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io), [Jest](https://jestjs.io/) and [Supertest](https://github.com/visionmedia/supertest) against Frontend and Backend API.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
