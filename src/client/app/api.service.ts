import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { IActivity } from './activities/list';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    alert('error connection to the Calculation API Endpoint.');
    return throwError('Something bad happened; please try again later.');
  };

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  /**
   * Posts the array of activities
   *
   *  @param {IActivity.Model[]} data Id of account
   * @param {IActivity.Options} options currently supports shuffle
   * @returns Observable of domain model
   */

  postActivities(
    data: IActivity.Model[],
    options: IActivity.Options
  ): Observable<any> {
    return this.http.post(apiUrl + '/accommodate', {data,options}, httpOptions)
      .pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
  }

}
