
export namespace IActivity {
    export interface Model {
        title: string,
        duration: number,
      }
      export interface Options {
        shuffle?: boolean,
      }
}



