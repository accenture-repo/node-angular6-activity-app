import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, FormArray, FormControl, FormGroup, NgForm, Validators, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IActivity } from './list';




@Component({
  selector: 'app-activity-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  model: IActivity.Model;
  shuffle: boolean;
  importData: IActivity.Model[];
  responseTime: number;
  exportData: any[];
  activities: IActivity.Model[];
  activityForm: FormGroup;
  activitiesOverallDuration: number;
  activitiesFormattedOverallDuration: string;

  activities$: Observable<IActivity.Model[]>;
  private SPRINT_DURATION: number = 15;
  private TIME_FORMAT: string = 'min';


  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {

    this.shuffle = false;
    this.activities = new Array();
    this.importData = new Array();
    this.exportData = new Array();
    this.activitiesOverallDuration = 0;
    this.activityForm = this.formBuilder.group({
      activityFormArray: new Array(),      
      shuffle: new FormControl(this.shuffle),

    });
     

    this.model = {
      'title': null,
      'duration': null,
    };
    //this.activityForm.controls.activityFormArray = this.transformActivityFormArray();
  }
  get activityFormArray(): FormArray {
    return (this.activityForm.get('activityFormArray') as FormArray);
  }

  
  transformActivityFormArray(): FormArray {
    let activityRows = [];
    this.activitiesOverallDuration = 0;
    this.activities.forEach((activity: IActivity.Model, i: number) => {
      const controls = {
        title: new FormControl(activity.title, Validators.required),
        duration: new FormControl(activity.duration)
      }
      this.activitiesOverallDuration = this.activitiesOverallDuration + activity.duration;
      activityRows.push(new FormGroup(controls));
    });
    this.activitiesFormattedOverallDuration = this.formatOverallDuration(this.activitiesOverallDuration);

    return new FormArray(activityRows);
  }
  addActivity(): void {
    this.activities.push(this.model);
    this.activityForm.controls.activityFormArray = this.transformActivityFormArray();
  }
  shuffleChange(): void {
    this.shuffle = this.activityForm.controls.shuffle.value;
  }
  rowChanged(index: number): void {
    const activity = this.activities[index];

    const rowControls = this.activityFormArray.controls;
    const title: string = rowControls[index].get('title').value;
    const duration: number = parseInt(rowControls[index].get('duration').value);

    // Changs populated
    this.activities[index] = { title, duration };

    // reset
    this.activitiesOverallDuration = 0;
    this.activitiesOverallDuration = this.activities.reduce((n, activity) => {
      return activity.duration ? n + activity.duration : n;
    }, 0);
    this.activitiesFormattedOverallDuration = this.formatOverallDuration(this.activitiesOverallDuration);

  }
  formatOverallDuration(minutes: number): string {
    return minutes ? moment.duration(minutes, 'minutes').humanize() : 'not yet calculated.';
  }
  fileUpload(event): void {

    const reader = new FileReader();
    reader.readAsText(event.srcElement.files[0]);
    reader.onload = () => {
      if(reader.result){
        this.importData = new Array();
        this.activities = new Array();
        this.transformRawImportData(reader.result);
      }
    }
  }
  transformRawImportData(rawImportData: any): void {
    //
    // import exercise format
    //
    const data = rawImportData.split(/\r?\n/);
    if (data.length) {
      data.forEach((item: string) => {

        const search = item.match(/([0-9]+min\b)|sprint\b/);
        const title = item.replace(' ' + search[0], '');
        const duration = this.transformDuration(search[0]);
        this.importData.push({ title, duration });

      });
      this.activities = this.importData;
      this.activityForm.controls.activityFormArray = this.transformActivityFormArray();
    }
  }
  transformDuration(strip: string): number {
    return strip && strip === 'sprint' ? this.SPRINT_DURATION : parseInt(strip.replace(this.TIME_FORMAT, ''));
  }
  onFormSubmit(form: NgForm): void {
    
    const sendDate = (new Date()).getTime()
    console.log('activityForm', this.activityForm);
    const options: IActivity.Options = {
      shuffle: this.shuffle
    }
    this.api.postActivities(this.activities, options).subscribe(res => {
      console.log('calculation api response:', res);
      const receiveDate = (new Date()).getTime();
      const responseTimeMs = receiveDate - sendDate;      
      if (res && res.length) {
        this.responseTime = responseTimeMs;
        this.exportData = res;
      }
    }, (err) => {
      console.log(err);
    });


    //this.exportData = teams;



  }
}


