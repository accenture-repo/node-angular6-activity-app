const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cors = require('cors');

// const mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/mean-angular6')
//   .then(() =>  console.log('MongoDB Connection Succesful'))
//   .catch((err) => console.error(err));

const apiRouter = require('./routes/index');
const app = express();
const frontend = path.join(__dirname, '../../dist/mean-angular6');
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(frontend));

app.use('/activity', express.static(frontend));
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.sendStatus(err.status);
});


module.exports = app;
