const request = require('supertest');
const app = require('../../../bin/www');
//agent = request.agent(app);
describe('Test the API calculation endpoint', () => {
  test('Sending test JSON data to the accommodate endpoint. Should response with the processed JSON data', done => {
    request(app)
      .post('/api/accommodate')
      .send({
        'data': [{
          'title': 'Duck',
          'duration': 60
        }],
        'options': {
          'shuffle': true
        }
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.text).toMatch(
          "[[{\"title\":\"Duck\",\"duration\":60,\"activityEnd\":60,\"start\":\"09:00 am\",\"finish\":\"10:00 am\"},{\"title\":\"Staff Motivation Presentation\",\"duration\":0,\"start\":\"05:00 pm\"}]]"
        );
        done();
      });
  });


  test('Sending zero signal agains calculation API, should respone 204 no content.', done => {
    request(app)
      .post('/api/accommodate')
      .send(null)
      .set('Accept', 'application/json')
      .expect(204)
      .then(response => {
    
        done();
      });
  });

});

app.close();