const express = require('express');
const router = express.Router();
const accommodate = require('../services/accommodate');

/**
 * API routes should always use nouns as resource identifiers
 * @returns void
 */

router.post('/accommodate', (req, res, next) => {
  if (req.body && req.body.data && req.body.data.length) {
    console.log('calculation endpoint is processing data.')
    const stream = req.body;
    const response = accommodate(stream.data, stream.options);
    res.set('Content-Type', 'application/json');
    res.status(200);
    return res.json(response);
  } else {
    console.log('zero signal')

    res.status(204);
    return res.json();
  }
});

module.exports = router;
