const moment = require('moment');
const accommodate = (activities, options) => {

  if(!activities){
    return {};
  }
  const teamActivitiesStart = moment.duration(9, 'hours');
  let teams = [];
  let team = 0;
  let container = [];
  const addLunchTime = true;

  const lunch = {
    title: 'Lunch Break',
    duration: 60,
    start: null,
    finish: null
  }
  const presentation = {
    title: 'Staff Motivation Presentation',
    duration: 0,
    start: "05:00 pm"
  }
  const initialTime = 0;
  let activityEnd = initialTime;

  // SORT ACTIVITIES
  const makeDayTime = (minutes) => {

    return moment().startOf('day').add(teamActivitiesStart).add(minutes, 'minutes').format("hh:mm a")

  };
  const shuffle = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }
  activities = options.shuffle && options.shuffle===true ? shuffle(activities) : activities;

  activities.forEach((activity, i) => {

    if (addLunchTime) {

      const lunchTimeEnd = moment.duration(activityEnd + lunch.duration, 'minutes').asHours();
      if (lunchTimeEnd > 3 && lunchTimeEnd <= 4) {
        lunch.start = makeDayTime(activityEnd);
        lunch.finish = makeDayTime(activityEnd + lunch.duration);
        container.push(lunch);
        activityEnd = activityEnd + lunch.duration;
      }

    }

    activityEnd = activityEnd + activity.duration;
    const newTeam = moment.duration(activityEnd, 'minutes').asHours() > 8 ? true : false;
    const last$ = i === (activities.length - 1);

    if (newTeam) {
      activityEnd = initialTime + activity.duration;
      team = team + 1;
      container = new Array();
    }

    const finish = makeDayTime(activityEnd, 'minutes');
    const start = makeDayTime(activityEnd - activity.duration);
    container.push({
      title: activity.title,
      duration: activity.duration,
      activityEnd: activityEnd,
      start: start,
      finish: finish
    });
    teams[team] = container;

    if (newTeam || last$) {
      newTeam ? teams[team - 1].push(presentation) : teams[team].push(presentation);
    }


  });
  return teams;
}
module.exports = accommodate;